package br.ucsal;

import java.util.Scanner;

public class DetectarPrimo {

	public static void main(String[] args) {
		executar();

	}

	public static void executar() {
		int n = solicitarNumero();
		boolean x = verificarNumero(n);
		apresentarResultado(x, n);
	}
	
	public static int solicitarNumero() {
		boolean intervalo = false;
		Scanner sc = new Scanner(System.in);
		int n = 0;
		
		while(!intervalo) {
			System.out.println("Informe um n�mero entre 1 e 1000");
			n = sc.nextInt();
			if(n > 0 && n < 1001) {
				intervalo = true;
			}
		}
		sc.close();
		return n;
	}
	
	public static boolean verificarNumero(int n) {
		for (int i = 2; i < n; i++) {
			if((n % i == 0) && (i != n)) {
				return false;
			}
		}
		return true;
	}
	
	public static void apresentarResultado(boolean x, int n) {
		if(x) {
			System.out.println("O n�mero " + n + " � primo!");
		}else {
			System.out.println("O n�mero " + n + " n�o � primo!");
		}
	}
}
