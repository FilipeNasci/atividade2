package br.ucsal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

public class OutputTest {

	@Test
	public void OutputTest1() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		DetectarPrimo dp = new DetectarPrimo();
		String expected = "O n�mero 45 n�o � primo!\r\n";
		dp.apresentarResultado(false, 45);
		String output = out.toString();
		Assert.assertEquals(expected, output);
	}
	
}
