package br.ucsal;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class DetectarPrimoTest {
	
	public boolean expected;
	public int input;
	
	public DetectarPrimo dp;
	
	@Before
	public void setup() {
		dp = new DetectarPrimo();
	}
	
	public DetectarPrimoTest(boolean expected,int input) {
		this.expected = expected;
		this.input = input;
	}
	
	@Parameters
	public static Collection<Object[]> testData(){
		Object[][] data = new Object[][] { {true, 2},{true, 5},{true, 7},{false, 8},{false, 15},{true, 11},{false, 12},{true, 23},{false, 25},{false, 26}};
		return Arrays.asList(data);
		
	}
	
	@Test
	public void testAdd() {
		Assert.assertEquals(expected, dp.verificarNumero(input));
	}
	
}
