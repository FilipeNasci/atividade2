package br.ucsal;

import java.io.ByteArrayInputStream;

import org.junit.Assert;
import org.junit.Test;

public class InputTeste {

	@Test
	public void testInput() {
		ByteArrayInputStream in = new ByteArrayInputStream("45".getBytes());
		System.setIn(in);
		DetectarPrimo dp = new DetectarPrimo();
		int n = dp.solicitarNumero();
		int expected = 45;
		Assert.assertEquals(expected, n);
		
	}
}
